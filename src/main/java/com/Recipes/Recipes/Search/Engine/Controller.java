package com.Recipes.Recipes.Search.Engine;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.*;
import java.util.stream.Collectors;

@RestController
public class Controller {

    @Autowired
    private JdbcTemplate jdbcTemplate;


    //Küsime kõik ingredientid ja salvestame need objektidena listi.
    @GetMapping("/ingredients")
    public List<Ingredient> getIngredients() {
        return getAllIngredients();
    }


    private List<Ingredient> getAllIngredients() {

        List<Ingredient> ingredients = jdbcTemplate.query("select * from ingredient",
                (row, count) -> {
                    int id = row.getInt("ID");
                    String recipeName = row.getString("IngredientName");

                    Ingredient ingredient = new Ingredient(recipeName, id);
                    return ingredient;

                }
        );
        return ingredients;
    }



    // Keyword järgi otsime õiged ingrediendid andmebaasist ja salvestame Listi nende id-d
    private List<Integer> getIngredientIdsByKeyword(String[] keyword) {

        List<Integer> ingredients = new ArrayList<>();
        List<Ingredient> allIngredients = getAllIngredients();

        for (int i = 0; i < allIngredients.size(); i++) {
            for (int j = 0; j < keyword.length; j++) {
                if (allIngredients.get(i).getName().equalsIgnoreCase(keyword[j])) {
                    ingredients.add(allIngredients.get(i).getId());
                }
            }
        }
        return ingredients;
    }



    @Autowired
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;


    // Kasutame ingredientide id Listi ja otsime selle abil retsepte, kus leidub sama id-ga ingrediente
    @GetMapping("/recipes")
    public Collection<Recipe> getRecipes(@RequestParam String keywords) {

        List<Integer> ingredientIds = getIngredientIdsByKeyword(keywords.split(","));
        MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue("ingredientID", ingredientIds);

        List<Recipe> recipes = namedParameterJdbcTemplate.query(
                "SELECT recipeingredient.ingredientID,  recipe.recipeName,  recipe.recipeURL, recipe.ID, recipe.recipeImage "
                        + "FROM recipe "
                        + "JOIN recipeingredient ON recipe.ID = recipeingredient.recipeID "
                        + "WHERE recipeingredient.ingredientID IN (:ingredientID)",
                parameters,
                (row, count) -> {
                    int id = row.getInt("ID");
                    String recipeName = row.getString("recipeName");
                    String recipeUrl = row.getString("recipeURL");
                    String recipeImage = row.getString("recipeImage");

                    Recipe recipe = new Recipe(recipeName, recipeUrl, id, recipeImage);
                    return recipe;
                }
        );


        // Vaatame kas recipeMap-s on antud id-ga retsepte, kui ei, siis lisame sinna ja suurendame loendurit ühe võrra.
        //Vastasel juhul võtame selle retsepti id järgi ja suurendame tema loendurit.
        Map<Integer, Recipe> recipesMap = new HashMap<>();

        for (Recipe r : recipes) {
            if (!recipesMap.containsKey(r.getId())) {
                recipesMap.put(r.getId(), r);
                Recipe storedRecipe = recipesMap.get(r.getId());
                storedRecipe.setIngredientMatchCount(storedRecipe.getIngredientMatchCount() + 1);
            } else {
                Recipe storedRecipe = recipesMap.get(r.getId());
                storedRecipe.setIngredientMatchCount(storedRecipe.getIngredientMatchCount() + 1);
            }

        }

        return recipesMap.values().stream().sorted((a, b) -> b.getIngredientMatchCount() - a.getIngredientMatchCount()).collect(Collectors.toList());

//        ArrayList<Recipe> sortedRecipes = new ArrayList<>(recipesMap.values());
//        sortedRecipes.sort((a, b) -> a.getIngredientMatchCount() - b.getIngredientMatchCount());
//
//        return sortedRecipes;

    }



    // Märksõna järgi retsepti pealkirja otsimine
    @GetMapping("/recipe")
    public Map<Integer, Recipe> getRecipeByName(String[] keywords) {

        List<Recipe> recipes = jdbcTemplate.query(
                "SELECT * FROM recipe",
                (row, count) -> {
                    int id = row.getInt("ID");
                    String recipeName = row.getString("recipeName");
                    String recipeUrl = row.getString("recipeURL");
                    String recipeImage = row.getString("recipeImage");

                    Recipe recipe = new Recipe(recipeName, recipeUrl, id, recipeImage);
                    return recipe;
                }
        );


        Map<Integer, Recipe> recipesMap = new HashMap<>();

        for (int i = 0; i < recipes.size(); i++) {
            for (int j = 0; j < keywords.length; j++) {
                if (recipes.get(i).getName().contains(keywords[j])) {
                    Recipe storedRecipe = recipesMap.put(recipes.get(i).getId(), recipes.get(i));
                }
            }
        }
        return  recipesMap;

    }


}
