package com.Recipes.Recipes.Search.Engine;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RecipesSearchEngineApplication {

	public static void main(String[] args) {
		SpringApplication.run(RecipesSearchEngineApplication.class, args);
	}

}

